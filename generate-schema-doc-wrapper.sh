#!/bin/bash

SCHEMA_FILE="$1"
# html should go to public/doc instead of public/
# so replace the path in the filename
RESULT_FILE="./public/doc/${SCHEMA_FILE##*/}"
# html should have tht .html suffix instead of .json
# so replace the suffix in the filename
RESULT_FILE="${RESULT_FILE%.*}.html"

generate-schema-doc --config-file ./.jsfh-conf.yaml "$SCHEMA_FILE" "$RESULT_FILE"

# remove timestamp
sed -i 's~</a> on \([0-9]\{2,4\}-\)\{2\}[0-9]\{2\} at \([0-9]\{2\}:\)\{2\}[0-9]\{2\} [0-9+Z]*</p>~</a></p>~g' "$RESULT_FILE"
# remote trailing whitespace
sed -i 's/\s*$//g' "$RESULT_FILE"
