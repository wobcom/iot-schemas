module gitlab.com/wobcom/iot-schemas

go 1.15

require (
	github.com/rs/zerolog v1.20.0
	github.com/xeipuuv/gojsonschema v1.2.0
)
