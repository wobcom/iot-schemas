package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"github.com/xeipuuv/gojsonschema"
)

const (
	SchemaURL = "http://json-schema.org/draft-07/schema"
	SchemaDir = "./public"
)

func main() {
	// nolint: exhasutivestruct
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	schema := gojsonschema.NewReferenceLoader(SchemaURL)

	fls, err := ioutil.ReadDir(SchemaDir)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	for _, i := range fls {
		logger := log.With().Str("path", i.Name()).Logger()
		fullPath := path.Join(SchemaDir, i.Name())

		if !i.Mode().IsRegular() {
			logger.Info().Msg("no file")

			continue
		}

		if filepath.Ext(i.Name()) != ".json" {
			logger.Info().Msg("no JSON file")

			continue
		}

		docRaw, err := ioutil.ReadFile(fullPath)
		if err != nil {
			logger.Fatal().Msg(err.Error())
		}

		doc := gojsonschema.NewBytesLoader(docRaw)

		result, err := gojsonschema.Validate(schema, doc)
		if err != nil {
			logger.Fatal().Msg(err.Error())
		}

		if result.Valid() {
			logger.Info().Msg("valid")

			continue
		} else {
			var errs []string
			errs = append(errs, fmt.Sprintln("Input is invalid, following errors found:"))

			for _, desc := range result.Errors() {
				errs = append(errs, fmt.Sprintf("- %s", desc))
			}
			logger.Fatal().Msg(strings.Join(errs, " "))
		}
	}
}
