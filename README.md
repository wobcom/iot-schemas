# IOT Schemas

Each device sends different data.  
Therefore each device needs a different datamodel which the decoder of the
device's payload should build.  
To ensure a homogeneous output for all sensors, no matter which manufacturer
they are from, we define the characteristics of all fields in some base schema
and reuse this fields in the json schema of each device.  
With this logic we can ensure that the field names and units the decoders
create are constant over all devices.  

For example the file `Base.json` contains the definition `AtmosphericPressure`.  
This definition defines a field called `atomosphericPressure` which consists of
a value of type `number` with unit `hPa`.  
If a specific device gives those values in `kPa` unit it's the job of the
payload decoder to convert from `kPa` to `hPa` to meet the requirements of
the data model.
